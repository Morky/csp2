const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('./../auth');


router.post('/register', (req, res)=>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.post('/login', (req, res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.get('/myOrders', auth.verifyToken, (req, res) => {
	let userData = auth.decodeAccessToken(req.headers.authorization);
	userController.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController));
})


router.get("/orders", auth.verifyToken, (req, res) => {
	let userData = auth.decodeAccessToken(req.headers.authorization);
	userController.getAllOrders(userData.id).then(resultFromController => res.send(resultFromController));
})



router.post('/checkout', auth.verifyToken, (req, res) => {
	let userData = auth.decodeAccessToken(req.headers.authorization);
	userController.checkout(userData.id, req.body)
	.then(resultFromController => res.send(resultFromController));
})



module.exports = router;