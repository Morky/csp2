const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


// Get All Products
router.get("/all", (req, res) => {
    productController.getAll().then(resultFromController => res.send(resultFromController));
})


// Get All Active Products
router.get("/active", auth.verifyToken, (req, res) => {
	let userData = auth.decodeAccessToken(req.headers.authorization);
    productController.getAllActive(userData.id).then(resultFromController => res.send(resultFromController));
})


//Add Products (Admin Only)
router.post("/", auth.verifyToken, (req, res) => {
	let userData = auth.decodeAccessToken(req.headers.authorization);
	productController.addProduct(userData.id, req.body).then(resultFromController => res.send(resultFromController));
})


// Get Specific Product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})


// Update Product
router.put("/:productId", auth.verifyToken, (req, res) => {
	let userData = auth.decodeAccessToken(req.headers.authorization);
	productController.updateProduct(userData.id, req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})


//Archive Product
router.put("/:productId/archive", auth.verifyToken, (req, res) => { 
	let userData = auth.decodeAccessToken(req.headers.authorization);
	productController.archiveProduct(userData.id, req.params.productId).then(resultFromController => res.send(resultFromController));
})


//Unarchive or Activate Product
router.put("/:productId/activate", auth.verifyToken, (req, res) => { 
	let userData = auth.decodeAccessToken(req.headers.authorization);
	productController.activateProduct(userData.id, req.params.productId).then(resultFromController => res.send(resultFromController));
})



module.exports = router;