const jwt = require('jsonwebtoken');
const secret = 'secret1234!';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user.id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}


module.exports.decodeAccessToken = (token) => {
	let slicedToken = token.slice(7, token.length);
	return jwt.decode(slicedToken);
}


module.exports.verifyToken = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== 'undefined') {
		let slicedToken = token.slice(7, token.length);
		return jwt.verify(slicedToken, secret, (err, data) => {
			if (err) {
				res.send({auth: 'failed'})
			} else {
				next();
			}
		})
	} else {
		res.send(false);
	}
}