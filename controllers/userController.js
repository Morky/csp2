const User = require('../models/user');
const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.registerUser = (reqBody) => {
	const newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10),
		// isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((result, error) => {
		if (result) {
			return result;
		} else {
			return error;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result, error) => {
		if (result == null) {
			return `Email not found!`;
		} else {
			let isPwCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPwCorrect) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false;
			}
		}
	})
}


module.exports.getMyOrders = (userId) => {
	return User.findById(userId).then((result, error) => {
		if (result.isAdmin == true) {
			return `You must be a regular user to see your orders!`;
		} else {
			return User.find({$and: [{_id: result.id}, {orders: result.orders}]}).then((res, err) => {
				if (res) {
					return res[0].orders;
				} else {
					return err;
				}
			})
		}
	})
}


module.exports.getAllOrders = (userId) => {
	return User.findById(userId).then((result, error) => {
		if (result.isAdmin == false) {
			return `Please log in as an administrator to view all orders!`;
		} else {
			return User.find({},{'email': 1, 'orders': 1}).then((res, err) => {
				if (res) {
					return res;
				} else {
					return err;
				}
			})
		}
	})
}



module.exports.checkout =  (userId, reqBody) => {
	return User.findById(userId).then((result, error) => {
		if (result.isAdmin == true) {
			return `You must be a regular user to proceed with checkout!`;
		} else {
			result.orders.push({products: reqBody.products, totalAmount: reqBody.totalAmount});
			return result.save().then((res, err) => {
				if (res) {
					const currentOrder = res.orders[res.orders.length-1];
					// console.log(currentOrder);
					currentOrder.products.forEach(product => {
						Product.findById(product.productId).then((foundProduct => {
							foundProduct.orders.push({orderId:currentOrder._id});
							foundProduct.save();
						}))
					})
					return true;
				} else {
					return false;
				}
			})
		}
	})

	// const productArray = reqBody.products;
	// let productId;
	// productArray.forEach(product => {
	// 	productId = JSON.stringify(product.productId);
	// });
	// const productOrder = await Product.findById(JSON.parse(productId)).then((result, error) => {
	// 	if (error) {
	// 		return err;
	// 	} else {
	// 		result.orders.push({orderId: });
	// 		return result.save().then(res => {
	// 			return true;
	// 		})
	// 	}
	// })
	// if (userOrder && productOrder) {
	// 	return true;
	// } else {
	// 	return false;
	// }
}