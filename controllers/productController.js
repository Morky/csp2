const Product = require('../models/product');
const User = require('../models/user');

module.exports.getAll = () => {
	return Product.find({}).then((result, error) => {
		if (result) {
			return result;
		} else {
			return error;
		}
	})
}


module.exports.getAllActive = (userId) => {
	return User.findById(userId).then((result, error) => {
		if (result.isAdmin == true) {
			return Product.find({isActive:true}).then((res, err) => {
				if (res) {
					return res;
				} else {
					return err;
				}
			})
		} else {
			return `Please log in as an admin to see all active products!`;
		}
	})
	// return Product.find({isActive:true}).then((result, error) => {
	// 	if (result) {
	// 		return result;
	// 	} else {
	// 		return error;
	// 	}
	// })
}


module.exports.addProduct = (userId, reqBody) => {
	return User.findOne({_id: userId}).then((result, error) => {
		if (result.isAdmin == true) {
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newProduct.save().then((res, err) => {
				if (res) {
					return res;
				} else {
					return err;
				}
			})
		} else {
			return `Please log in as an admin to add a product!`;
		}
	})
}


module.exports.getProduct = (productId) => {
	return Product.findOne({_id: productId}).then((result, error) => {
		if (result) {
			return result;
		} else {
			return error;
		}
	})
}


module.exports.updateProduct = (userId, productId, reqBody) => {
	return User.findOne({_id: userId}).then((result, error) => {
		if (result.isAdmin == false) {
			return `Please log in as an administrator to update a product!`
		} else {
			const updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			};
			return Product.findByIdAndUpdate(productId, updatedProduct, {returnDocument: 'after'}).then((res, err) => {
				if (res) {
					return res;
				} else {
					return err;
				}
			})
		}
	})
}


module.exports.archiveProduct = (userId, productId) => {
	return User.findById(userId).then((result, error) => {
		if (result.isAdmin == false) {
			return `Please log in as an administrator to archive a product!`;
		} else {
			return Product.findByIdAndUpdate(productId, {isActive: false}, {returnDocument: 'after'}).then((res, err) => {
				if (res) {
					return res;
				} else {
					return err;
				}
			})
		}
	})
}



module.exports.activateProduct = (userId, productId) => {
	return User.findById(userId).then((result, error) => {
		if (result.isAdmin == false) {
			return `Please log in as an administrator to activate a product!`;
		} else {
			return Product.findByIdAndUpdate(productId, {isActive: true}, {returnDocument: 'after'}).then((res, err) => {
				if (res) {
					return res;
				} else {
					return err;
				}
			})
		}
	})
}