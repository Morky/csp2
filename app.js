// Get dependencies/modules
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const cors = require('cors');

// Server setup
const app = express();
let port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Connect to MongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.qrju6.mongodb.net/csp2?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Routes
app.use('/products', productRoutes);
app.use('/users', userRoutes);


// Server listening
app.listen(port,() => console.log(`Listening to port ${port}`));


app.listen(process.env.PORT || 3001, () => console.log('Server is running, catch if you can!'));